NODEJS Container:

1. Check folder "Workshop-7-DockerFile" by command
	ls ~/Workshop-7-DockerFile

2. Build docker image from command: 
	docker image build -t akenarongake/node:1.0 ~/Workshop-7-DockerFile/nodejs

3. Test run docker image from command: "docker container run -dt --name NODEJS -p 3000:3000 akenarongake/node:1.0"

4. open url:"http://<Public IP>:3000"

5. Clean Up Lab with command below

	11.1 "docker container stop NODEJS"
	11.2 "docker container rm NODEJS"